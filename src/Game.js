const RouletteEngine = require('./RouletteEngine')
const readline = require('readline')
const UserModel = require('./UserModel')
const Utils = require('./Utils')
const DraftLog = require('draftlog').into(console)

class Game {

  users // All the users as Map<number, UserModel>
  rl // The readline interface to capture user input
  engine // The roulette engine

  timerLogger // The updatable timer log

  lockEntry = false // Locking entry will not allow more bids

  /**
   * Listen for user input, passing in the name of the users playing
   * @param {Map<number,UserModel>} users All the users playing
   */
  constructor(users) {
    // Create a new readline interface, binding to the console IN and OUT
    this.rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    })

    this.users = users
    this.engine = new RouletteEngine()

    this.startTicker()
    this.welcomePlayers()

    process.stdin.on('keypress', (str, key) => {
      if (key.name === 'escape') {
        this.executeCommand('cancel')
      }
    })
  }

  /**
   * Create the main UI, and all the console drafts needing to be updated
   */
  welcomePlayers() {
    const title =
      'Welcome to the Roulette Game! When placing a bet, you can either use your Name or ID'

    this.dispTitle(title)
    this.draftSubtitle()

    // Set the users draft for logging
    this.users.forEach((val) => val.setDraft(console.draft()))

    this.dispSubtitle(``)
    this.dispSubtitle(``)
    this.dispSubtitle(`Please place your bets using the following format:`)
    this.dispSubtitle(`{ID / Name} {even/odd/1-36} {value}`)
    this.dispSubtitle(``)
    this.dispSubtitle(`Type /stats to show user stats, press [esc] to go back`)
    this.dispBreak(title.length)

    this.resetRound()
    this.captureBets()
  }

  /**
   * Start the round ticker, pausing when there are no bets, and resetting the round on compeltion.
   * Betting is paused during the last few seconds.
   */
  startTicker() {
    let roundLimit = 30
    let pauseTimer = false
    let pauseLimit = 3
    const timeLogger = console.draft('Timer: 30')

    const ix = setInterval(() => {
      timeLogger(
        `Timer: ${pauseTimer ? '0' : roundLimit}       ${
          this.lockEntry ? '(Entries Locked)' : ''
        }${pauseTimer ? '(Resetting Round)' : ''}${
          this.engine.noBets ? '(Round will start when someone bets)' : ''
        }`
      )

      if (this.engine.noBets) {
        return
      }

      if (!pauseTimer) {
        roundLimit--
      } else {
        pauseLimit--
        if (pauseLimit < 0) {
          pauseLimit = 3
          pauseTimer = false
          this.resetRound()
        }
      }

      if (roundLimit < 0) {
        roundLimit = 30
        pauseTimer = true

        this.engine.fetchWinners(this.users)
      }

      if (roundLimit <= 5) {
        this.lockEntry = true
      } else {
        this.lockEntry = false
      }
    }, 1000)

    this.rl.addListener('SIGINT', () => {
      // We're here because the user wants to exit. Stop the interval, pause readline, and exit
      // This is so we can cleanly exit without hiccups or memory leaks
      clearInterval(ix)
      this.rl.pause()
      process.exit()
    })
  }

  /**
   * Recursive function to capture user bets
   */
  async captureBets() {
    const message = await this.askQuestion('')
    if (message.startsWith('/')) {
      this.executeCommand(message)
      this.captureBets()
      return
    }
    if (this.lockEntry) {
      this.captureBets()
      return
    }
    const parts = message.split(' ')

    // Check to see if we have the right amount of parts:
    // - name
    // - bet
    // - amount
    if (parts.length === 3) {
      let user = null
      let id = parseInt(parts[0])
      if (isNaN(id)) {
        user = this.getUserByName(parts[0])
      } else {
        user = this.users.get(id)
      }

      if (user === null || !user) {
        this.captureBets()
        return
      }

      this.placeUserBet(user, parts[1], parts[2])
    }

    this.captureBets()
  }

  /**
   * Places a bet
   * @param {UserModel} user The user to place the bet on
   * @param {string} part0 The BET parameter
   * @param {string} part1 The AMOUNT parameter
   */
  placeUserBet(user, part0, part1) {
    const bet = part0.toLowerCase()
    const value = part1.toLowerCase()

    let validBet = false
    // Check to see if the bet is even or odd
    if (bet === 'even' || bet === 'odd') {
      validBet = true
    }

    // If it's not the above, check to see if the bet is a number between 1 and 36
    if (!validBet) {
      const betValue = parseInt(bet)
      if (isNaN(betValue)) {
        // Invalid bet value!
        return
      }
      if (betValue < 1 || betValue > 36) {
        // Bet value is too high!
        return
      }
    }

    const valueFloat = parseFloat(value) // Parse the value to a float
    if (isNaN(valueFloat) || valueFloat <= 0) {
      // Bet value invalid!
      return
    }

    // We're here because we want to place bets
    this.engine.addBet(user.id, bet, valueFloat)
    user.addBet(bet, valueFloat)
  }

  /**
   * Pose a question to a user
   * @param {string} question Question for the user to answer
   */
  async askQuestion(question) {
    return new Promise((res, rej) => {
      this.rl.question(question, (answer) => {
        readline.moveCursor(process.stdout, 0, -1)
        res(answer)
      })
    })
  }

  /**
   * Resets the round, clears the bets
   */
  resetRound() {
    this.users.forEach((value) => value.clearBets())
    this.engine.clearBets()
  }

  /**
   * Find a User by their Name
   * @param {string} name Name of the user
   */
  getUserByName(name) {
    let found = null
    this.users.forEach((value) => {
      if (found !== null) {
        return
      }
      if (value.name === name) {
        found = value
      }
    })
    return found
  }

  /**
   * **** The below functions are internal for display / command usage
   */
  commands = {
    '/stats': () => this.commandShowStats(),
    '/statscancel': () => this.commandHideStats()
  }
  currentCommand = null
  subtitleDraft = null
  showStatus = false

  executeCommand(command) {
    if (command === 'cancel') {
      if (this.currentCommand !== null) {
        this.commands[`${this.currentCommand}cancel`]()
      }
      this.currentCommand = null

      return
    }

    this.currentCommand = command
    try {
      this.commands[command]()
    } catch (e) {}
  }

  commandShowStats() {
    this.users.forEach((value) => value.setShowStats(true))
    this.showStatus = true
    this.draftSubtitle()
  }

  commandHideStats() {
    this.users.forEach((value) => value.setShowStats(false))
    this.showStatus = false
    this.draftSubtitle()
  }

  draftSubtitle() {
    const dispId = Utils.fitInSpace('ID', Utils.idPadding)
    const dispName = Utils.fitInSpace('NAME', Utils.namePadding)

    if (this.subtitleDraft === null) {
      this.subtitleDraft = new console.draft()
    }

    let dispCurrentMode = 'BETS'
    if (this.showStatus) {
      dispCurrentMode = `${Utils.fitInSpace(
        'TOTAL WIN',
        Utils.totalWinPadding
      )}${Utils.fitInSpace('TOTAL BET', Utils.totalBetPadding)}`
    }

    this.subtitleDraft(`║ ${dispId}${dispName}${dispCurrentMode}`)
  }

  dispTitle(text) {
    console.log(`╔═══ ${text} ═══╗`)
  }

  dispSubtitle(text) {
    console.log(`║ ${text}`)
  }

  dispPlayerDraft(text) {
    return console.draft(`║ ${text}`)
  }

  dispBreak(length) {
    console.log(`╚════${'═'.repeat(length)}════╝`)
  }
}

module.exports = Game

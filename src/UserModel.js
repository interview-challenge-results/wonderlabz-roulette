const Utils = require('./Utils')

class UserModel {
  id
  name
  totalWin
  totalBet

  draft = null
  bets = []
  roundWin = null
  calculateComplete = false

  dispId = ''
  dispName = ''

  showStats = false

  /**
   *
   * @param {number} id ID of the user
   * @param {string} name Display name of the user
   * @param {number} totalWin Total cumulative win
   * @param {number} totalBet Total cumulative bet
   */
  constructor(id, name, totalWin, totalBet) {
    this.id = id
    this.name = name
    this.totalWin = totalWin
    this.totalBet = totalBet

    this.dispId = Utils.fitInSpace('' + id, Utils.idPadding)
    this.dispName = Utils.fitInSpace(name, Utils.namePadding)
  }

  addBet(bet, value) {
    this.totalBet += value
    this.bets.push({
      bet,
      value
    })

    this.updateDraft()
  }

  /**
   * Calculate the winnings of the user (and display results) based
   * off the random number drawn
   * @param {number} number Random number that has been drawn
   */
  calculateWinnings(number) {
    const directNumberHits = this.bets.filter((x) => x.bet === `${number}`)
    let evenHits = []
    let oddHits = []

    if (number % 2 == 0) {
      evenHits = this.bets.filter((x) => x.bet.toLowerCase() === `even`)
    } else {
      evenHits = this.bets.filter((x) => x.bet.toLowerCase() === `odd`)
    }

    this.roundWin = 0
    for (const bet of directNumberHits) {
      this.roundWin += bet.value * 36
    }
    for (const bet of evenHits) {
      this.roundWin += bet.value * 2
    }
    for (const bet of oddHits) {
      this.roundWin += bet.value * 2
    }

    this.totalWin += this.roundWin

    this.calculateComplete = true

    this.updateDraft()
  }

  clearBets() {
    this.bets = []
    this.calculateComplete = false
    this.updateDraft()
  }

  /**
   * Set the display object for this user
   * @param {any} draft The console output draft
   */
  setDraft(draft) {
    this.draft = draft
    this.updateDraft()
  }

  /**
   * Optionally shows and hides the user stats display
   * @param {boolean} show True to show stats
   */
  setShowStats(show) {
    this.showStats = show
    this.updateDraft()
  }

  /**
   * Update the display for this user in the console
   */
  updateDraft() {
    if (this.draft === null) {
      return
    }

    if (this.calculateComplete) {
      this.draftCalculationResults()
    } else {
      if (this.showStats) {
        this.draftUserStats()
      } else {
        this.draftRoundStatus()
      }
    }
  }

  draftUserStats() {
    const dispTotalWin = Utils.fitInSpace(
      this.totalWin + '',
      Utils.totalWinPadding
    )
    const dispTotalBet = Utils.fitInSpace(
      this.totalBet + '',
      Utils.totalBetPadding
    )

    this.draft(`║ ${this.dispId}${this.dispName}${dispTotalWin}${dispTotalBet}`)
  }

  draftRoundStatus() {
    const extra = this.bets.map((x) => `${x.bet}[${x.value}]`).join(' ') || '-'
    this.draft(`║ ${this.dispId}${this.dispName}${extra}`)
  }

  draftCalculationResults() {
    this.draft(
      `║ ${this.id}    ${
        this.roundWin > 0 ? '!!! WINNER !!!          ' + this.roundWin : ''
      }`
    )
  }
}

module.exports = UserModel

class Utils {
  // Default padding
  namePadding = 10
  idPadding = 4
  totalWinPadding = 10
  totalBetPadding = 10

  /**
   * Formats a string to fit within a defind space
   * @param {string} text Text to fit in
   * @param {number} space Spaces to fit in
   */
  fitInSpace(text, space) {
    if (text.length > space) {
      return text.substr(0, 7) + '..  '
    }

    const padding = space - text.length
    return text + ' '.repeat(padding + 1)
  }
}

module.exports = new Utils()

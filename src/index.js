const fs = require('fs')
const Game = require('./Game')
const UserModel = require('./UserModel')

// First thing we want to do is read the names inside the data file
fs.readFile('./data/users.txt', 'utf8', (err, data) => {
  if (err) {
    console.error('users.txt not found')
    return
  }

  const lines = data.split(/\r?\n/)
  const users = createAccounts(lines)

  new Game(users)
})

function createAccounts(lines) {
  const users = new Map()
  let id = 0

  for (const line of lines) {
    const split = line.split(',')
    const name = split[0]
    const win = split[1] || '0'
    const bet = split[2] || '0'

    let winParse = parseFloat(win)
    if (isNaN(winParse)) {
      winParse = 0.0
    }

    let betParse = parseFloat(bet)
    if (isNaN(betParse)) {
      betParse = 0.0
    }

    users.set(id, new UserModel(id, name, winParse, betParse))
    id++
  }

  return users
}

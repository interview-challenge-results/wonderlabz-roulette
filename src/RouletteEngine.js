class RouletteEngine {
  roundBets = []

  get noBets() {
    return this.roundBets.length === 0
  }

  addBet(index, number, amount) {
    this.roundBets.push({
      index,
      number,
      amount
    })
  }

  clearBets() {
    this.roundBets = []
  }

  /**
   * Fetch the winning users from the array and update their status
   * @param {Map<number, UserModel>} users Map of users
   */
  fetchWinners(users) {
    const randomNumber = this.randomIntFromInterval(0, 36)
    if (randomNumber > 0) {
      users.forEach((value) => value.calculateWinnings(randomNumber))
    }
  }

  randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }
}

module.exports = RouletteEngine

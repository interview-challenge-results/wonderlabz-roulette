# Roulette (Cross Platform) Terminal Game
![main-interface](/images/main-interface.png)

All loaded player accounts are listed on the **main screen** when starting the application, showing you their ID, Name, and Bet details. The game will remain paused until at least one bet is submitted.  

---  

![make-bet](/images/make-bet.png)

To make a bet, type in the following command:  
`{name / id} {even/odd/1-36} {amount}`  

You can use a User's name or ID to place a bet, either bet 'even', 'odd', or any number from 1-36, as well as include the amount you want to bet.  
A user can bet multiple times per round

---  

![post-play](/images/post-play.png)

A few seconds before the round completes, all bets will be closed. When a round completes the UI will display all the winners and the value they've won.

The game will reset and accept new bets a few seconds after the round has completed.

---  

![stats](/images/stats.png)

Typing the following command will display the user stats:  
`/stats`  

Press `esc` to go back to the previous screen

---  

## Running
In order to run this game, you will need NodeJS (v12) installed on your system.
Open a terminal instance in the root of this project, and run the following commands:  
`npm i` to install all necessary libraries  
`npm start` to start the game  


Alternatively, you can run the game by downloading and running the binary file (linked below) for your system.


## Links
Windows Executable File:  
https://drive.google.com/file/d/15TlKIMAWWPSD4P82L7BjwQt7Lw_lavdC/view?usp=sharing

MacOS Executable File:  
https://drive.google.com/file/d/1ZTGI7MJTeoYsU2Bpg5Q8vldxSlqZa4AP/view?usp=sharing